package urma.default_methods;

/**
 * Created by Adam on 7/15/2015.
 */
public interface Fightable {

    //adds a java 8 default method
    default void punch() {
        System.out.println("from Fightable - punch");
    }
}
